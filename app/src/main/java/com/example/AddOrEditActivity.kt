package com.example
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.net.toUri
import com.example.roomtest.R
import com.example.roomtest2.RoomLayer.DB.AppDatabase
import com.example.roomtest2.RoomLayer.Entities.HadesehEntity
import com.example.roomtest2.Utilities.ImageUtil
import kotlinx.android.synthetic.main.activity_add_or_edit.*

class AddOrEditActivity : AppCompatActivity() {

    public var MODE_ADD = 1
    public var MODE_EDIT = 2
    private var mode = MODE_ADD
    private var id = 0

    lateinit var editHadeseh: HadesehEntity

    lateinit var db: AppDatabase

    val context = this

    private val pickRequest = 1

    private var imageURi: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_or_edit)

        db = AppDatabase.getInstance(context)
        val id = intent.getIntExtra("id", 0)
        if (id == 0)
            mode = MODE_ADD
        else {
            mode = MODE_EDIT
            this.id = intent.getIntExtra("id", 0)
            editHadeseh = db.havades().getSingle(id)
        }

        init()
        listeners()

    }

    private fun listeners() {

        actv_aoe_submit.setOnClickListener {
            if (validator()) {
                val tul = actv_aoe_tul.text.toString().toInt()
                val arz = actv_aoe_arz.text.toString().toInt()
                var join = tul*arz
                val imageName = saveImage()

                if (mode == MODE_ADD)
                    addrecord(tul, arz,join, imageName)

                if (mode == MODE_EDIT)
                    editrecord(editHadeseh.id, tul, arz,join,imageName)

            }
        }

        actv_aoe_user_img.setOnClickListener {
            openPicker()
        }

    }

    private fun init() {
        when (mode) {
            MODE_ADD -> {
                actv_aoe_user_img.setImageResource(R.drawable.no_photo)
                actv_aoe_submit.text = "افزودن"
                title = "افزودن"
            }
            MODE_EDIT -> {
                actv_aoe_submit.text = "ویرایش"
                viewsInitializer(editHadeseh)
                title = "ویرایش ${editHadeseh.tul}"
            }
        }
    }

    private fun viewsInitializer(hadeseh: HadesehEntity) {

        actv_aoe_tul.setText(hadeseh.tul)
        actv_aoe_arz.setText(hadeseh.arz)
        textView2.setText(hadeseh.Zarb)
        loadUserImage(hadeseh.image)

    }

    fun loadUserImage(imageName: String?) {

        if (imageName != null) {
            val imageFile = ImageUtil.loadFilePrivate(context, imageName)
            actv_aoe_user_img.setImageURI(imageFile.toUri())
        }
    }

    fun validator(): Boolean {

        //TODO() // write validator code

        return true;
    }

    private fun openPicker() {
        val pick = Intent(Intent.ACTION_GET_CONTENT)
        pick.setType("image/*")
        startActivityForResult(pick, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == pickRequest && resultCode == Activity.RESULT_OK) {
            imageURi = data!!.data!!
            changeImage(imageURi!!)
        }

    }

    private fun changeImage(data: Uri) {
        actv_aoe_user_img.setImageURI(data)
    }

    private fun saveImage(): String? {

        var result: String? = null
        if (imageURi != null) {
            result = ImageUtil.saveFilePrivate(context, imageURi!!)
        }

        return result
    }

    private fun addrecord(
        tul: Int,
        arz: Int,
        Join:Int,
        imageName: String? = null

    ) {

        val addrecord = HadesehEntity(tul, arz,Join, imageName)
        db.havades().insert(addrecord)
        finish()

    }

    private fun editrecord(
        id: Int,
        tul: Int, arz: Int, Join:Int, imageName: String? = null
    ) {
        var imgName = imageName

        if (imgName == null || imgName == "")
            imgName = editHadeseh.image

        val editrecord = HadesehEntity(id , tul,  arz,Join, imgName)
        db.havades().update(editrecord)
        finish()

    }

}
