package com.example.roomtest2.RoomLayer.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "havades")
data class HadesehEntity(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "tul") var tul: Int,
    @ColumnInfo(name = "arz") var arz: Int,
    @ColumnInfo(name = "Zarb") var Zarb: Int,
    @ColumnInfo(name = "user_image") var image: String? = null
) {
    constructor(tul: Int, arz: Int, Zarb: Int, image: String? = null) : this(
        0,
        tul,
        arz,
        Zarb,
        image
    )
}