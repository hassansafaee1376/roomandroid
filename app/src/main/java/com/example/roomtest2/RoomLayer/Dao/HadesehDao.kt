package com.example.roomtest2.RoomLayer.Dao
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.roomtest2.RoomLayer.Entities.HadesehEntity
@Dao
interface HadesehDao {
    @Query("select * from havades")
    fun getAll():List<HadesehEntity>
    @Query("select * from havades where id =:id")
    fun getSingle(id:Int):HadesehEntity

    @Insert
    fun insert (havades:HadesehEntity)

    @Update
    fun update(havades: HadesehEntity)
}