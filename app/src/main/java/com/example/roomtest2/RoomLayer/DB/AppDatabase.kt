package com.example.roomtest2.RoomLayer.DB
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.roomtest2.RoomLayer.Dao.HadesehDao
import com.example.roomtest2.RoomLayer.Entities.HadesehEntity
@Database(entities = arrayOf(HadesehEntity::class),version = 1)
abstract class AppDatabase :RoomDatabase(){
    abstract fun havades() :HadesehDao
        companion object {
            private var instance:AppDatabase?=null
            fun getInstance(context: Context):AppDatabase{
                if (instance==null){
                    instance=Room.databaseBuilder(context,AppDatabase::class.java,"Room_db")
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
                }
                return instance!!
        }
    }
}