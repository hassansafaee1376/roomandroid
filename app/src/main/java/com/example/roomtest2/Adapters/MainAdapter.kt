package com.example.roomtest2.Adapters
import com.example.roomtest2.Utilities.ImageUtil
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.example.AddOrEditActivity
import com.example.roomtest.R
import com.example.roomtest2.RoomLayer.Entities.HadesehEntity
import kotlinx.android.synthetic.main.model_main_list.view.*

class MainAdapter(val context: Activity, val list: List<HadesehEntity>) :
    RecyclerView.Adapter<MainAdapter.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.model_main_list, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MainAdapter.MyHolder, position: Int) {
        val view=holder.itemView
        val hadeseh = list[position]
        val card = view.mo_main_card
        val tul = view.mo_main_user_name
        val arz = view.mo_main_user_name2
        val hadesehImage = view.mo_main_user_img
        card.setBackgroundResource(R.drawable.bg_main_list)
        tul.text=hadeseh.tul.toString()
        arz.text=hadeseh.arz.toString()
        if (hadeseh.image!=null){
            val imageFile = ImageUtil.loadFilePrivate(context,hadeseh.image!!)
            hadesehImage.setImageURI(imageFile.toUri())
        }
        else{
            hadesehImage.setImageResource(R.drawable.no_photo)
        }
        card.setOnClickListener{
            val open = Intent(context,AddOrEditActivity::class.java)
            open.putExtra("id",hadeseh.id)
            context.startActivity(open)
        }
    }
    class MyHolder(itemView: View):RecyclerView.ViewHolder(itemView)
}