package com.example.roomtest2
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.AddOrEditActivity
import com.example.roomtest.R
import com.example.roomtest2.Adapters.MainAdapter
import com.example.roomtest2.RoomLayer.DB.AppDatabase
import com.example.roomtest2.RoomLayer.Entities.HadesehEntity
import com.example.roomtest2.Utilities.ImageUtil
import com.google.android.material.snackbar.Snackbar

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {
    lateinit var db:AppDatabase
    val context = this
    val pickRequest =1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        db = AppDatabase.getInstance(context)

        listeners()
        recyclerInit()


        }

    private fun recyclerInit(){
        actv_main_rcy_havades.itemAnimator=DefaultItemAnimator()
        actv_main_rcy_havades.layoutManager=LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        loadHavades()
    }
    private fun loadHavades(){
        val havades = getHavades()
        val havadesAdapter= MainAdapter(context,havades)
        actv_main_rcy_havades.adapter=havadesAdapter
    }
    private fun getHavades():List<HadesehEntity>{
        val havades = db.havades().getAll()
        return havades
    }
    private fun listeners(){
        fab.setOnClickListener{
            openAddOrEdit()
        }
    }
    fun advancedAddHavades(){
        val picker = Intent(Intent.ACTION_GET_CONTENT)
        picker.setType("image/*")
        startActivityForResult(picker,pickRequest)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==pickRequest&&resultCode==Activity.RESULT_OK){
            val uImage =data!!.data!!
             val SaveImage = ImageUtil.saveFilePrivate(context,uImage)
    }

    }

    fun addHadeseh(tul:Int, arz:Int, Zarb:Int, image:String?){
        db.havades().insert(HadesehEntity(tul,arz,Zarb,image))
    }

    private fun openAddOrEdit(id:Int=0){
        val open = Intent(context,AddOrEditActivity::class.java)
        if(id!=0)
            open.putExtra("id",id)
        startActivity(open)
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onRestart(){
        super.onRestart()
        loadHavades()
    }
}
